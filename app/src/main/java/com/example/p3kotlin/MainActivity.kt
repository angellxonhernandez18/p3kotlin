package com.example.p3kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var weightEditText: EditText
    private lateinit var heightEditText: EditText
    private lateinit var resultTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        weightEditText = findViewById(R.id.weightEditText)
        heightEditText = findViewById(R.id.heightEditText)
        resultTextView = findViewById(R.id.resultTextView)

        val calculateButton: Button = findViewById(R.id.calculateButton)
        calculateButton.setOnClickListener {
            calculateBMI()
        }

        val clearButton: Button = findViewById(R.id.clearButton)
        clearButton.setOnClickListener {
            clearFields()
        }

        val closeButton: Button = findViewById(R.id.closeButton)
        closeButton.setOnClickListener {
            finish()
        }
    }

    private fun calculateBMI() {
        val weightString = weightEditText.text.toString()
        val heightString = heightEditText.text.toString()

        if (weightString.isNotEmpty() && heightString.isNotEmpty()) {
            val weight = weightString.toFloat()
            val height = heightString.toFloat()

            val bmi = weight / (height * height)
            val category = getBMICategory(bmi)
            val result = String.format("IMC: %.2f\nCategoría: %s", bmi, category)
            resultTextView.text = result
        } else {
            resultTextView.text = "Por favor, ingresa el peso y la altura"
        }
    }


    private fun clearFields() {
        weightEditText.text.clear()
        heightEditText.text.clear()
        resultTextView.text = ""
    }

    private fun getBMICategory(bmi: Float): String {
        return when {
            bmi < 18.5 -> "Bajo peso"
            bmi < 25 -> "Peso normal"
            bmi < 30 -> "Sobrepeso"
            else -> "Obesidad"
        }
    }
}
